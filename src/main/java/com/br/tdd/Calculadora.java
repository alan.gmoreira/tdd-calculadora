package com.br.tdd;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Calculadora {
    public static int soma(int a, int b) {
        return a + b;
    }

    public static Double dividir(Double a, Double b) {
        BigDecimal retorno = BigDecimal. valueOf(a);

        retorno = retorno.divide(BigDecimal.valueOf(b),2, RoundingMode.FLOOR);

        return retorno.doubleValue();
    }

    public static Double multiplicar(Double a, Double b) {
        BigDecimal retorno = BigDecimal. valueOf(a);

        retorno = retorno.multiply(BigDecimal.valueOf(b)).setScale(2,RoundingMode.FLOOR);

        return retorno.doubleValue();
    }
}
