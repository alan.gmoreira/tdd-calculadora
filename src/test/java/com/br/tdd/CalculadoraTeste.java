package com.br.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class CalculadoraTeste {

    @Test
    public void testarSomaDeDoisNumeros()
    {
        int resultado = Calculadora.soma(2,2);

        Assertions.assertEquals(4, resultado);
    }

    @Test
    public void testarDivisaoDeNumerosInteiros()
    {
        //BigDecimal a = BigDecimal.valueOf(10);
        Double a = 10.0;
        Double b = 3.0;

        Double resultado = Calculadora.dividir(a,b);

        Assertions.assertEquals(3.33, resultado);
    }

    @Test
    public void testarDivisaoDeNumerosFlutuantes()
    {
        //BigDecimal a = BigDecimal.valueOf(10);
        Double a = 10.62521634;
        Double b = 3.524334234;

        Double resultado = Calculadora.dividir(a,b);

        Assertions.assertEquals(3.01, resultado);
    }

    @Test
    public void testarDivisaoPorZero()
    {
        //BigDecimal a = BigDecimal.valueOf(10);
        Double a = 10.62521634;
        Double b = 0.0;

        Assertions.assertThrows(ArithmeticException.class, () ->  Calculadora.dividir(a,b));
    }

    @Test
    public void testarMultiplicacaoPorNumerosInteiros()
    {
        //BigDecimal a = BigDecimal.valueOf(10);
        Double a = 10.0;
        Double b = 3.0;

        Double resultado = Calculadora.multiplicar(a,b);

        Assertions.assertEquals(30.0, resultado);
    }

    @Test
    public void testarMultiplicacaoPorNumerosFlutuante()
    {
        //BigDecimal a = BigDecimal.valueOf(10);
        Double a = 10.62521634;
        Double b = 3.524334234;

        Double resultado = Calculadora.multiplicar(a,b);

        Assertions.assertEquals(37.44, resultado);
    }
}
